from dzr_shared_util.db.db_conn import DBConnector

from api.async_api_handler import BatchRunner, async_api
from consts import logger
from orch.icr_siru_runner import ICR_SIRU_Runner

KNOWN_MODEL_RUNNER_IMPLS = [
    ICR_SIRU_Runner
]


def get_all_model_runners():
    return KNOWN_MODEL_RUNNER_IMPLS


def get_model_runner(model_id):
    models_with_id = list(
        filter(lambda model_runner: model_runner.get_model_id() == model_id, KNOWN_MODEL_RUNNER_IMPLS))
    if len(models_with_id) == 0:
        raise ValueError("Unknown model_id: " + str(model_id))
    if len(models_with_id) > 1:
        raise ValueError("Multiple models with model_id: " + str(model_id))
    return models_with_id[0]


class AsyncBatchModelRunner(BatchRunner):

    def __init__(self, return_id_field_name="task_id", model_ids=None) -> None:
        super().__init__(return_id_field_name)
        if model_ids is None:
            self.model_runners = get_all_model_runners()
        else:
            self.model_runners = [get_model_runner(model_id) for model_id in model_ids]

    @async_api
    def _batch_run(self, task_id):
        # perform some intensive processing
        logger.info(
            "Starting batch processing of models " + ", ".join(
                [str(model.get_model_id()) for model in self.model_runners]))
        # todo: move this out of the batch processing to benefit from parallelizing calls to multiple models by
        #  running a batch process per model
        for model_runner in self.model_runners:
            db = DBConnector()
            try:
                runner = model_runner(db)
                runner.batch_run()
            finally:
                db.close_connection()
            logger.info("completed processing model " + str(model_runner.get_model_id()))
        logger.info("completed processing all models")
        return "complete"


batch_runner = None


def batch_run(model_ids=None):
    global batch_runner
    try:
        logger.debug("Creating asynchronous batch model runner...")
        batch_runner = AsyncBatchModelRunner("batch_id", model_ids)
        logger.debug("Running asynchronous batch model runner...")
        return batch_runner.batch_run()
    except Exception as e:
        return {"Error": str(e)}, 500


def batch_status(batch_id):
    global batch_runner
    try:
        if batch_runner is None:
            batch_runner = AsyncBatchModelRunner("batch_id")
        return batch_runner.batch_status(batch_id)
    except Exception as e:
        return {"Error": str(e)}, 500
