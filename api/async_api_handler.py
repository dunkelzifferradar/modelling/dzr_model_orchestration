import threading
import uuid
from abc import ABC, abstractmethod
from datetime import datetime
from functools import wraps
from http.client import HTTPException

from flask import current_app, request
from werkzeug.exceptions import HTTPException, abort

from consts import logger

tasks = {}


def async_api(wrapped_function):
    @wraps(wrapped_function)
    def new_function(*args, **kwargs):
        def task_call(flask_app, environ, task_id):
            # Create a request context similar to that of the original request
            # so that the task can have access to flask.g, flask.request, etc.
            with flask_app.request_context(environ):
                try:
                    tasks[task_id]['return_value'] = wrapped_function(*args, **kwargs)
                except HTTPException as e:
                    tasks[task_id]['return_value'] = current_app.handle_http_exception(e)
                except Exception as e:
                    tasks[task_id]['return_value'] = str(e)
                finally:
                    # We record the time of the response, to help in garbage
                    # collecting old tasks
                    tasks[task_id]['completion_timestamp'] = datetime.timestamp(datetime.utcnow())

                    # close the database session (if any)

        # Record the task, and then launch it
        task_id = args[1]
        tasks[task_id]['task_thread'] = threading.Thread(target=task_call,
                                                         args=(current_app._get_current_object(),
                                                               request.environ,
                                                               task_id))
        tasks[task_id]['task_thread'].start()

        # Return a 202 response, with an id that the client can use to obtain task status
        return {tasks[task_id]['return_id_field_name']: task_id}, 202

    return new_function


class BatchRunner(ABC):

    def __init__(self, return_id_field_name="task_id") -> None:
        self.return_id_field_name = return_id_field_name

    def batch_run(self):
        task_id = uuid.uuid4().hex
        logger.debug("Assigned id " + str(task_id) + " to the asynchronous task")
        global tasks
        tasks[task_id] = {}
        tasks[task_id]['return_id_field_name'] = self.return_id_field_name
        return self._batch_run(task_id)

    @abstractmethod
    @async_api
    def _batch_run(self, task_id):
        """
        Implement this method in a subclass to run code asynchronously.
        Important: add the @async_api decorator to the implementation method as well!
        :param task_id: the task ID that the asynchronous processing uses to store results and other info. No need to
        do anything with it in the method implementation - it is being taken care of in the async_api wrapper.
        :return: the result to be shown as API call output.
        """
        pass

    @staticmethod
    def batch_status(task_id):
        """
        Return status about an asynchronous task. If this request returns a 202
        status code, it means that task hasn't finished yet. Else, the response
        from the task is returned.
        """
        task = tasks.get(task_id)
        if task is None:
            abort(404)
        if 'return_value' not in task:
            return {"progress": "ongoing"}, 202
        return {"progress": str(task['return_value'])}, 200
