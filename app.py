""" Module containing basic connexion backend. """

import connexion
from dotenv import load_dotenv
from flask_cors import CORS
from orch.model_batch_runner import batch_run, batch_status

load_dotenv(".env")

app = connexion.App(
    __name__, options={"swagger_ui": True}  # os.environ.get("SWAGGER_UI", False)}
)
CORS(app.app)
app.add_api("app.yml")
application = app.app

if __name__ == "__main__":
    app.run(port=8079, server="gevent")
