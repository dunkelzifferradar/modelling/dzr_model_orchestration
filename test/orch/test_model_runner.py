import unittest
from unittest.mock import patch

from dotenv import load_dotenv
from dzr_shared_util.db.db_conn import DBConnector
from pandas import read_csv

from consts import ROOT_DIR, DIM_TBL_STATES, DIM_TBL_COUNTRIES
from orch.model_runner import ModelRunner

load_dotenv(ROOT_DIR + "/.env")


def mock_get_data(tbl_name, where=""):
    where = where.replace("=", "==").replace(">==", ">=").replace("<==", "<=")
    samples_df = read_csv(ROOT_DIR + '/test/data/' + tbl_name + '.csv', sep=',')
    if where == "":
        return samples_df
    df_where_res = samples_df.query(where)
    return df_where_res


class TestImplModelRunner(ModelRunner):
    MODEL_ID = 4711

    def batch_run(self):
        pass

    def get_model_id(self):
        return self.MODEL_ID


class TestSomeModel(unittest.TestCase):

    def setUp(self):
        self.conn = DBConnector()
        self.cut = TestImplModelRunner(self.conn)

    @patch('dzr_shared_util.db.db_conn.DBConnector.get_data', side_effect=mock_get_data)
    def test_get_model_params(self, mock_get_data_method):
        result = self.cut._get_model_params()
        self.assertEqual(2, len(result))
        exp_cols = {"IFR_VAL", "INFECTIOUS (SYMPTOMATIC)", "INFECTIOUS (ASYMPTOMATIC)", 'MODEL_CONFIG_ID'}
        self.assertEqual(exp_cols, set(result.columns))

    @patch('dzr_shared_util.db.db_conn.DBConnector.get_data', side_effect=mock_get_data)
    def test_get_counties(self, mock_get_data_method):
        result = self.cut._get_targets("COUNTY")
        self.assertIsInstance(result, list)
        exp_row_count = 10
        self.assertEqual(exp_row_count, len(result))
        result = self.cut._get_targets("STATE")
        self.assertIsInstance(result, list)
        exp_row_count = 16
        self.assertEqual(exp_row_count, len(result))
        result = self.cut._get_targets("COUNTRY")
        self.assertIsInstance(result, list)
        exp_row_count = 1
        self.assertEqual(exp_row_count, len(result))

    def tearDown(self):
        self.conn.close_connection()


if __name__ == '__main__':
    unittest.main()
