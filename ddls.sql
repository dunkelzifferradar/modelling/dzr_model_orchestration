CREATE TABLE SOR_DIM_MODEL (
    ID smallint,
    NAME varchar(20),
    DESCRIPTION varchar(128),
    PRIMARY KEY (ID),
    UNIQUE(NAME)
);

INSERT INTO SOR_DIM_MODEL VALUES (1, 'ICR_SIRU', 'Based on SIRU epidemological model. As one input for it, ICR (Infection-to-Case Ratio) is computed.');

CREATE TABLE SOR_DIM_MODEL_CONFIG (
    ID smallint,
    MODEL_ID smallint,
	DESCRIPTION varchar(128),
    PRIMARY KEY (ID),
    FOREIGN KEY (MODEL_ID) REFERENCES SOR_DIM_MODEL (ID)
);
COMMENT ON TABLE SOR_DIM_MODEL_CONFIG IS 'Table of prepared model parameter configs. Has detailed param tables ("SOR_DIM_MODEL_PARAM...") pointing to it via Foreign Keys';
INSERT INTO SOR_DIM_MODEL_CONFIG VALUES(0, 1, 'MODEL=ICR_SIRU; IFR=HEINSBERG; INFECTIOUS (SYMPTOMATIC)=7; INFECTIOUS (ASYMPTOMATIC)=7');
INSERT INTO SOR_DIM_MODEL_CONFIG VALUES(1, 1, 'MODEL=ICR_SIRU; IFR=LANCET; INFECTIOUS (SYMPTOMATIC)=7; INFECTIOUS (ASYMPTOMATIC)=7');
INSERT INTO SOR_DIM_MODEL_CONFIG VALUES(2, 1, 'MODEL=ICR_SIRU; IFR=IOANNIDIS; INFECTIOUS (SYMPTOMATIC)=7; INFECTIOUS (ASYMPTOMATIC)=7');

CREATE TABLE SOR_DIM_MODEL_PARAM_IFR (
    ID smallint,
    MODEL_CONFIG_ID smallint,
    NAME varchar(16),
    VAL float,
    PRIMARY KEY (ID),
    FOREIGN KEY (MODEL_CONFIG_ID) REFERENCES SOR_DIM_MODEL_CONFIG(ID),
    UNIQUE(NAME)
);
COMMENT ON TABLE SOR_DIM_MODEL_PARAM_IFR IS 'Table of possible values for model parameter Infection-Fatality-Ratio (IFR)';
INSERT INTO SOR_DIM_MODEL_PARAM_IFR VALUES (0, 0, 'HEINSBERG', 0.00358);
INSERT INTO SOR_DIM_MODEL_PARAM_IFR VALUES (1, 1, 'LANCET', 0.00657);
INSERT INTO SOR_DIM_MODEL_PARAM_IFR VALUES(2, 2, 'IOANNIDIS', 0.0024);

CREATE TABLE SOR_DIM_MODEL_PARAM_TIME_TYPE (
    ID smallint,
    NAME varchar(32),
    UNIT varchar(8),
    PRIMARY KEY(ID),
    UNIQUE(NAME)
);
COMMENT ON TABLE SOR_DIM_MODEL_PARAM_TIME_TYPE IS 'Table of possible types of model parameters expressing time spans. Used in table SOR_DIM_MODEL_PARAM_TIME';
INSERT INTO SOR_DIM_MODEL_PARAM_TIME_TYPE VALUES (0, 'INFECTIOUS (SYMPTOMATIC)', 'DAYS');
INSERT INTO SOR_DIM_MODEL_PARAM_TIME_TYPE VALUES (1, 'INFECTIOUS (ASYMPTOMATIC)', 'DAYS');

CREATE TABLE SOR_DIM_MODEL_PARAM_TIME (
    ID smallint,
    MODEL_CONFIG_ID smallint,
    TYPE_ID smallint,
    VAL int,
    PRIMARY KEY (ID),
    FOREIGN KEY (MODEL_CONFIG_ID) REFERENCES SOR_DIM_MODEL_CONFIG(ID),
    FOREIGN KEY (TYPE_ID) REFERENCES SOR_DIM_MODEL_PARAM_TIME_TYPE(ID)
);
COMMENT ON TABLE SOR_DIM_MODEL_PARAM_TIME IS 'Table of possible values for model parameters that express time spans.';
INSERT INTO SOR_DIM_MODEL_PARAM_TIME VALUES (0, 0, 0, 7);
INSERT INTO SOR_DIM_MODEL_PARAM_TIME VALUES (1, 0, 1, 7);
INSERT INTO SOR_DIM_MODEL_PARAM_TIME VALUES (2, 1, 0, 7);
INSERT INTO SOR_DIM_MODEL_PARAM_TIME VALUES (3, 1, 1, 7);
INSERT INTO SOR_DIM_MODEL_PARAM_TIME VALUES (4, 2, 0, 7);
INSERT INTO SOR_DIM_MODEL_PARAM_TIME VALUES (5, 2, 1, 7);

CREATE VIEW DM_MODEL_PARAMS as
    SELECT cfg.ID as MODEL_CONFIG_ID,
        cfg.MODEL_ID  as MODEL_ID,
        prm_time.val as PRM_TIME_VAL,
        prm_time_type.name as PRM_TIME_TYPE,
        prm_ifr.val as IFR_VAL
    FROM SOR_DIM_MODEL_CONFIG cfg
    LEFT JOIN SOR_DIM_MODEL_PARAM_TIME prm_time
    ON cfg.ID = prm_time.MODEL_CONFIG_ID
    LEFT JOIN SOR_DIM_MODEL_PARAM_TIME_TYPE prm_time_type
    ON prm_time.type_id = prm_time_type.id
    LEFT JOIN SOR_DIM_MODEL_PARAM_IFR prm_ifr
    ON cfg.ID = prm_ifr.MODEL_CONFIG_ID;

-- Tables for ICR service results
CREATE TABLE SOR_MDL_ICR_COUNTY (
    TARGET_ID smallint,
    DATE date,
    GEO_AGGR boolean,
    DAYS_AGGR smallint,
    IFR_BASE float,
    EXP_VAL float,
    CI_LOWER float,
    CI_UPPER float,
    PRIMARY KEY (TARGET_ID, DATE, GEO_AGGR, DAYS_AGGR, IFR_BASE),
    FOREIGN KEY (TARGET_ID) REFERENCES SOR_DIM_COUNTY(ID)
);

CREATE TABLE SOR_MDL_ICR_STATE (
    TARGET_ID smallint,
    DATE date,
    GEO_AGGR boolean,
    DAYS_AGGR smallint,
    IFR_BASE float,
    EXP_VAL float,
    CI_LOWER float,
    CI_UPPER float,
    PRIMARY KEY (TARGET_ID, DATE, GEO_AGGR, DAYS_AGGR, IFR_BASE),
    FOREIGN KEY (TARGET_ID) REFERENCES SOR_DIM_STATE(ID)
);

CREATE TABLE SOR_MDL_ICR_COUNTRY (
    TARGET_ID smallint,
    DATE date,
    GEO_AGGR boolean,
    DAYS_AGGR smallint,
    IFR_BASE float,
    EXP_VAL float,
    CI_LOWER float,
    CI_UPPER float,
    PRIMARY KEY (TARGET_ID, DATE, GEO_AGGR, DAYS_AGGR, IFR_BASE),
    FOREIGN KEY (TARGET_ID) REFERENCES SOR_DIM_COUNTRY(ID)
);


CREATE TABLE SOR_MDL_RESULT_COUNTY (
	DATE date,
	MODEL_CONFIG_ID smallint,
	TARGET_ID smallint,
	CASES_NEW integer,
	CASES_ACTIVE integer,
	CASES_SUM integer,
	INFECTIONS_NEW integer,
	INFECTIONS_ACTIVE integer,
	INFECTIONS_SUM integer,
	INSERT_DATE timestamp,
	PRIMARY KEY(DATE, TARGET_ID, MODEL_CONFIG_ID),
	FOREIGN KEY(MODEL_CONFIG_ID) REFERENCES SOR_DIM_MODEL_CONFIG(ID),
	FOREIGN KEY(TARGET_ID) REFERENCES SOR_DIM_COUNTY(ID)
);

CREATE TABLE SOR_MDL_RESULT_STATE (
	DATE date,
	MODEL_CONFIG_ID smallint,
	TARGET_ID smallint,
	CASES_NEW integer,
	CASES_ACTIVE integer,
	CASES_SUM integer,
	INFECTIONS_NEW integer,
	INFECTIONS_ACTIVE integer,
	INFECTIONS_SUM integer,
	INSERT_DATE timestamp,
	PRIMARY KEY(DATE, TARGET_ID, MODEL_CONFIG_ID),
	FOREIGN KEY(MODEL_CONFIG_ID) REFERENCES SOR_DIM_MODEL_CONFIG(ID),
	FOREIGN KEY(TARGET_ID) REFERENCES SOR_DIM_STATE(ID)
);

CREATE TABLE SOR_MDL_RESULT_COUNTRY (
	DATE date,
	MODEL_CONFIG_ID smallint,
	TARGET_ID smallint,
	CASES_NEW integer,
	CASES_ACTIVE integer,
	CASES_SUM integer,
	INFECTIONS_NEW integer,
	INFECTIONS_ACTIVE integer,
	INFECTIONS_SUM integer,
	INSERT_DATE timestamp,
	PRIMARY KEY(DATE, TARGET_ID, MODEL_CONFIG_ID),
	FOREIGN KEY(MODEL_CONFIG_ID) REFERENCES SOR_DIM_MODEL_CONFIG(ID),
	FOREIGN KEY(TARGET_ID) REFERENCES SOR_DIM_COUNTRY(ID)
);

--Create indices for faster views
--create index idx_county_date on SOR_FACT_CASE(COUNTY_ID, REPORT_DATE);
create index idx_county_date_model_config_id on SOR_MDL_RESULT_COUNTY(TARGET_ID, DATE, MODEL_CONFIG_ID);
create index idx_state_date_model_config_id on SOR_MDL_RESULT_STATE(TARGET_ID, DATE, MODEL_CONFIG_ID);
create index idx_country_date_model_config_id on SOR_MDL_RESULT_COUNTRY(TARGET_ID, DATE, MODEL_CONFIG_ID);

--County-level View
create or replace view DM_INFECTIONS_COUNTY as
select TARGET_ID as ID,
	name,
	population,
	state_id,
	country_id,
	DATE,
    CASES_NEW,
    CASES_ACTIVE,
    CASES_SUM,
    INFECTIONS_NEW,
    INFECTIONS_ACTIVE,
    INFECTIONS_SUM,
    MODEL_ID,
    INFECT_SYMP as PARAM_INFECT_SYMP,
    INFECT_ASYMP as PARAM_INFECT_ASYMP,
    PARAM_IFR
from (
    select TARGET_ID, DATE, cases.MODEL_CONFIG_ID, CASES_NEW, CASES_ACTIVE, CASES_SUM,
        INFECTIONS_NEW, INFECTIONS_ACTIVE, INFECTIONS_SUM, county.NAME, county.population,
		state.id as state_id, country.id as country_id, model_id, infect_symp, infect_asymp, val as param_ifr
    from SOR_MDL_RESULT_COUNTY cases
    join SOR_DIM_COUNTY county
    on cases.TARGET_ID = county.id
    join SOR_DIM_STATE state
    on county.state_id = state.id
    join SOR_DIM_COUNTRY country
    on state.country_id = country.id
    left join (
        select cfg.ID, cfg.MODEL_ID, prm_time.val as INFECT_SYMP
        from SOR_DIM_MODEL_CONFIG cfg
        LEFT JOIN SOR_DIM_MODEL_PARAM_TIME prm_time
        ON cfg.ID = prm_time.MODEL_CONFIG_ID
        LEFT JOIN SOR_DIM_MODEL_PARAM_TIME_TYPE prm_time_type
        ON prm_time.type_id = prm_time_type.id
        where prm_time_type.name = 'INFECTIOUS (SYMPTOMATIC)'
    ) cfg_symp
    on cases.model_config_id = cfg_symp.id
    left join (
        select cfg.ID, prm_time.val as INFECT_ASYMP
        from SOR_DIM_MODEL_CONFIG cfg
        LEFT JOIN SOR_DIM_MODEL_PARAM_TIME prm_time
        ON cfg.ID = prm_time.MODEL_CONFIG_ID
        LEFT JOIN SOR_DIM_MODEL_PARAM_TIME_TYPE prm_time_type
        ON prm_time.type_id = prm_time_type.id
        where prm_time_type.name = 'INFECTIOUS (ASYMPTOMATIC)'
    ) cfg_asymp
    on cases.model_config_id = cfg_asymp.id
    LEFT JOIN SOR_DIM_MODEL_PARAM_IFR prm_ifr
    ON cfg_symp.ID = prm_ifr.MODEL_CONFIG_ID
) data;


--Country-level View
create or replace view DM_INFECTIONS_COUNTRY as
select TARGET_ID as ID,
    NAME,
    POPULATION,
	DATE,
    CASES_NEW,
    CASES_ACTIVE,
    CASES_SUM,
    INFECTIONS_NEW,
    INFECTIONS_ACTIVE,
    INFECTIONS_SUM,
    MODEL_ID,
    INFECT_SYMP as PARAM_INFECT_SYMP,
    INFECT_ASYMP as PARAM_INFECT_ASYMP,
    PARAM_IFR
from (
	select TARGET_ID, DATE, mdl_result.MODEL_CONFIG_ID, CASES_NEW, CASES_ACTIVE, CASES_SUM,
        INFECTIONS_NEW, INFECTIONS_ACTIVE, INFECTIONS_SUM, country.population, country.name,
        model_id, infect_symp, infect_asymp, val as param_ifr
	from SOR_MDL_RESULT_COUNTRY mdl_result
	join SOR_DIM_COUNTRY country
	on mdl_result.target_id = country.id
    join (
        select cfg.ID, cfg.MODEL_ID, prm_time.val as INFECT_SYMP
        from SOR_DIM_MODEL_CONFIG cfg
        LEFT JOIN SOR_DIM_MODEL_PARAM_TIME prm_time
        ON cfg.ID = prm_time.MODEL_CONFIG_ID
        LEFT JOIN SOR_DIM_MODEL_PARAM_TIME_TYPE prm_time_type
        ON prm_time.type_id = prm_time_type.id
        where prm_time_type.name = 'INFECTIOUS (SYMPTOMATIC)'
    ) cfg_symp
    on mdl_result.model_config_id = cfg_symp.id
    join (
        select cfg.ID, prm_time.val as INFECT_ASYMP
        from SOR_DIM_MODEL_CONFIG cfg
        LEFT JOIN SOR_DIM_MODEL_PARAM_TIME prm_time
        ON cfg.ID = prm_time.MODEL_CONFIG_ID
        LEFT JOIN SOR_DIM_MODEL_PARAM_TIME_TYPE prm_time_type
        ON prm_time.type_id = prm_time_type.id
        where prm_time_type.name = 'INFECTIOUS (ASYMPTOMATIC)'
    ) cfg_asymp
    on mdl_result.model_config_id = cfg_asymp.id
    LEFT JOIN SOR_DIM_MODEL_PARAM_IFR prm_ifr
    ON cfg_symp.ID = prm_ifr.MODEL_CONFIG_ID
) data;

--State-level View
create or replace view DM_INFECTIONS_STATE as
select
	TARGET_ID as ID,
	name,
	population,
	country_id,
	DATE,
    CASES_NEW,
    CASES_ACTIVE,
    CASES_SUM,
    INFECTIONS_NEW,
    INFECTIONS_ACTIVE,
    INFECTIONS_SUM,
    MODEL_ID,
    INFECT_SYMP as PARAM_INFECT_SYMP,
    INFECT_ASYMP as PARAM_INFECT_ASYMP,
    PARAM_IFR
from (
	select TARGET_ID, DATE, mdl_result.MODEL_CONFIG_ID, CASES_NEW, CASES_ACTIVE, CASES_SUM,
        INFECTIONS_NEW, INFECTIONS_ACTIVE, INFECTIONS_SUM, state.name, state.population, state.country_id,
        model_id, infect_symp, infect_asymp, val as param_ifr
	from SOR_MDL_RESULT_STATE mdl_result
	join SOR_DIM_STATE state
	on mdl_result.target_id = state.id
    join (
        select cfg.ID, cfg.MODEL_ID, prm_time.val as INFECT_SYMP
        from SOR_DIM_MODEL_CONFIG cfg
        LEFT JOIN SOR_DIM_MODEL_PARAM_TIME prm_time
        ON cfg.ID = prm_time.MODEL_CONFIG_ID
        LEFT JOIN SOR_DIM_MODEL_PARAM_TIME_TYPE prm_time_type
        ON prm_time.type_id = prm_time_type.id
        where prm_time_type.name = 'INFECTIOUS (SYMPTOMATIC)'
    ) cfg_symp
    on mdl_result.model_config_id = cfg_symp.id
    join (
        select cfg.ID, prm_time.val as INFECT_ASYMP
        from SOR_DIM_MODEL_CONFIG cfg
        LEFT JOIN SOR_DIM_MODEL_PARAM_TIME prm_time
        ON cfg.ID = prm_time.MODEL_CONFIG_ID
        LEFT JOIN SOR_DIM_MODEL_PARAM_TIME_TYPE prm_time_type
        ON prm_time.type_id = prm_time_type.id
        where prm_time_type.name = 'INFECTIOUS (ASYMPTOMATIC)'
    ) cfg_asymp
    on mdl_result.model_config_id = cfg_asymp.id
    LEFT JOIN SOR_DIM_MODEL_PARAM_IFR prm_ifr
    ON cfg_symp.ID = prm_ifr.MODEL_CONFIG_ID
) data;

DROP TABLE SOR_ARCH_MDL_RESULT;
CREATE TABLE SOR_ARCH_MDL_RESULT as
select * from SOR_MDL_RESULT_COUNTY limit 0;

DROP PROCEDURE ARCHIVE_MDL_RESULTS(mdl_config_id integer);
CREATE PROCEDURE ARCHIVE_MDL_RESULTS(mdl_config_id integer, result_level varchar)
LANGUAGE plpgsql
AS $$
BEGIN
    IF result_level = 'COUNTY' THEN
        INSERT INTO SOR_ARCH_MDL_RESULT
        SELECT * FROM SOR_MDL_RESULT_COUNTY
        WHERE MODEL_CONFIG_ID = mdl_config_id;
    ELSIF result_level = 'STATE' THEN
        INSERT INTO SOR_ARCH_MDL_RESULT
        SELECT * FROM SOR_MDL_RESULT_STATE
        WHERE MODEL_CONFIG_ID = mdl_config_id;
    ELSIF result_level = 'COUNTRY' THEN
        INSERT INTO SOR_ARCH_MDL_RESULT
        SELECT * FROM SOR_MDL_RESULT_COUNTRY
        WHERE MODEL_CONFIG_ID = mdl_config_id;
    ELSE
        RAISE EXCEPTION 'Invalid result_level: %', result_level
        USING HINT = 'Must be in {COUNTY, STATE, COUNTRY}.';
    END IF;
END;
$$;