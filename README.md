# dzr_model_orchestration
Orchestration of model projects. Provides batch run support across multiple models and parameter combinations.

Also takes care of running different model-related projects in the right sequence.

## Setup
### Environment variables
Set the following variables (e.g. via dotenv):
* DB_USER
* DB_PASSWORD
* DB_HOST
* DB_PORT
* DB_NAME
* DB_SSL_CERT (can be empty)
* DB_SSL_KEY (can be empty)
* ICR_SERVICE_ENDPOINT (full path to evaluation endpoint of hosted project dzr_model_icr)
* SIRU_SERVICE_ENDPOINT (full path to evaluation endpoint of hosted project dzr_model_sir)