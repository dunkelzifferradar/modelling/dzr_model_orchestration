from dotenv import load_dotenv
from dzr_shared_util.db.db_conn import DBConnector

from consts import TARGET_LEVELS, MODEL_RESULT_COUNTY_TBL, MODEL_RESULT_STATE_TBL, MODEL_RESULT_COUNTRY_TBL, \
    DIM_TBL_COUNTIES, DIM_TBL_STATES, DIM_TBL_COUNTRIES, ROOT_DIR

load_dotenv(ROOT_DIR + "/.env")


def get_model_tables(level):
    if level == "COUNTY":
        res_table = MODEL_RESULT_COUNTY_TBL
        pop_table = DIM_TBL_COUNTIES
    elif level == "STATE":
        res_table = MODEL_RESULT_STATE_TBL
        pop_table = DIM_TBL_STATES
    elif level == "COUNTRY":
        res_table = MODEL_RESULT_COUNTRY_TBL
        pop_table = DIM_TBL_COUNTRIES
    else:
        raise ValueError("Level must be in " + str(TARGET_LEVELS))
    return res_table, pop_table


# configure
model_config_id = 0  # can currently be 0 or 1, referring to Heinsberg or Lancet research IFR
quantiles = [round(0.1 * i, 1) for i in range(1, 10)]
quantile_col = "INFECTIONS_ACTIVE"
relative_per = 100000
# run
db = DBConnector()
try:
    df_all_levels = None
    for level in TARGET_LEVELS:
        res_table, pop_table = get_model_tables(level)
        df_model_results = db.get_data(res_table, where="MODEL_CONFIG_ID=" + str(model_config_id))
        df_population = db.get_data(pop_table)
        df_joined = df_model_results.join(df_population.set_index("ID"), on="TARGET_ID")
        quantile_relative_col = quantile_col + "_RELATIVE"
        df_joined[quantile_relative_col] = df_joined[quantile_col] * relative_per / df_joined["POPULATION"]
        df_joined.index.name = "QUANTILE"
        df_quantiles = df_joined[[quantile_col, quantile_relative_col]].copy()
        if df_all_levels is None:
            df_all_levels = df_quantiles
        else:
            df_all_levels = df_all_levels.append(df_quantiles)
        df_quantiles.quantile(quantiles).to_csv("quantiles_" + str(level) + ".csv")
    df_all_levels.index.name = "QUANTILE"
    df_all_levels.quantile(quantiles).to_csv("quantiles_all_levels.csv")
finally:
    db.close_connection()
