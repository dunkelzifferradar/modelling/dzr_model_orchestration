import unittest
from datetime import datetime
from unittest.mock import patch

from dotenv import load_dotenv
from dzr_shared_util.db.db_conn import DBConnector
from pandas import DataFrame, read_csv
from pandas._testing import assert_frame_equal

from consts import ROOT_DIR
from orch.icr_siru_runner import ICR_SIRU_Runner

load_dotenv(ROOT_DIR + "/.env")

ICR_RESPONSE_BODY = DataFrame({
    "ICR_CI_LOWER": [0.1, 0.2],
    "DATE": [datetime.strptime('2020-03-17', '%Y-%m-%d').date(), datetime.strptime('2020-03-18', '%Y-%m-%d').date()],
    "ICR_EXP_VAL": [0.3, 0.5],
    "TARGET_ID": [1111, 1112],
    "ICR_CI_UPPER": [0.8, 1]
})

SIRU_RESPONSE_BODY = DataFrame({
    "DATE": [datetime.strptime('2020-03-17', '%Y-%m-%d').date(), datetime.strptime('2020-03-18', '%Y-%m-%d').date()],
    "TARGET_ID": [1111, 1112],
    "NEW_REPORTED": [123, 351],
    "ACTIVE_REPORTED": [123, 474],
    "SUM_REPORTED": [123, 474],
    "NEW_INFECTIONS": [123, 474],
    "ACTIVE_INFECTIONS": [123, 474],
    "SUM_INFECTIONS": [123, 474]
})

run_param_set_counter = 0


def mock_icr_call(url, body=None, query_params=None, mode='GET'):
    return {
        "status_code": 200,
        "text": "OK",
        "body": ICR_RESPONSE_BODY.to_json(orient="table")
    }


def mock_siru_call(url, body=None, query_params=None, mode='GET'):
    return {
        "status_code": 200,
        "text": "OK",
        "body": SIRU_RESPONSE_BODY.to_json(orient="table")
    }


def mock_get_targets(level) -> list:
    return [1001, 1002]


def mock_get_model_params() -> DataFrame:
    return read_csv(ROOT_DIR + "/test/data/PARAMS_PIVOT.csv")


def mock_run_param_set(target_ids, target_level,
                       model_config_id, ifr, days_infectious_symptomatic, days_infectious_asymptomatic):
    global run_param_set_counter
    run_param_set_counter += 1


class TestICUSIRURunner(unittest.TestCase):

    def setUp(self):
        self.conn = DBConnector()
        self.cut = ICR_SIRU_Runner(self.conn)

    @patch('api.api_calls.call_endpoint', side_effect=mock_icr_call)
    def test_call_icr_service(self, mock_icr_call_method):
        result = self.cut.call_icr_service([1111, 1112], "COUNTY", 0.00358, 7, True, "seasonal")
        self.assertIsInstance(result, DataFrame)
        # built-in testing function from pandas. Returns None if no differences are found between dataframes.
        self.assertIsNone(assert_frame_equal(ICR_RESPONSE_BODY, result))

    @patch('orch.icr_siru_runner.ICR_SIRU_Runner._get_targets', side_effect=mock_get_targets)
    @patch('orch.icr_siru_runner.ICR_SIRU_Runner._get_model_params', side_effect=mock_get_model_params)
    @patch('orch.icr_siru_runner.ICR_SIRU_Runner.run_param_set', side_effect=mock_run_param_set)
    def test_batch_run(self, mock_get_counties_method, mock_get_model_params_method, mock_run_param_set_method):
        # set counter of nested method calls to 0
        global run_param_set_counter
        run_param_set_counter = 0
        NUMBER_OF_TARGET_LEVELS = 3
        # run test method
        self.cut.batch_run()
        # get the parameters dataframe for the number of rows, i.e. the number of expected calls to run_param_set method
        param_df = mock_get_model_params()
        self.assertEqual(len(param_df.index) * NUMBER_OF_TARGET_LEVELS, run_param_set_counter)

    @patch('api.api_calls.call_endpoint', side_effect=mock_siru_call)
    def test_call_siru_service(self, mock_siru_call_method):
        result = self.cut.call_siru_service([1111, 1112], "COUNTY", 7, 7, 0.00358, 7, True)
        self.assertIsInstance(result, DataFrame)
        # built-in testing function from pandas. Returns None if no differences are found between dataframes.
        self.assertIsNone(assert_frame_equal(SIRU_RESPONSE_BODY, result))

    def tearDown(self):
        self.conn.close_connection()


if __name__ == '__main__':
    unittest.main()
