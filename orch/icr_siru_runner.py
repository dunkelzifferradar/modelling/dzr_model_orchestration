import os

from dateutil.utils import today
from dotenv import load_dotenv
from dzr_shared_util.db.db_conn import DBConnector, DBQueryException
from pandas import DataFrame, read_json

from api import api_calls
from consts import ROOT_DIR, ICR_RESULTS_TBL_COUNTY, ICR_RESULTS_TBL_STATE, ICR_RESULTS_TBL_COUNTRY, \
    ICR_RESULTS_TBL_KEYS, logger, MODEL_RESULT_COUNTY_TBL, MODEL_RESULT_STATE_TBL, \
    MODEL_RESULT_COUNTRY_TBL
from orch.model_runner import ModelRunner

load_dotenv(ROOT_DIR + "/.env")


# todo: move into shared_util project
def prep_sql_in_subquery(lst: list):
    return "(" + ", ".join([str(elem) for elem in lst]) + ")"


class ICR_SIRU_Runner(ModelRunner):
    DAYS_AGGR = 7
    GEO_AGGR = True
    SMOOTHING = 'seasonal'

    def __init__(self, db: DBConnector) -> None:
        super().__init__(db)
        if os.environ.get('ICR_SERVICE_ENDPOINT') is None or os.environ.get('ICR_SERVICE_ENDPOINT') == "":
            raise EnvironmentError("Endpoint URL must be set in environment variable")
        self.url_icr_endpoint = os.environ.get('ICR_SERVICE_ENDPOINT')
        if os.environ.get('SIRU_SERVICE_ENDPOINT') is None or os.environ.get('SIRU_SERVICE_ENDPOINT') == "":
            raise EnvironmentError("Endpoint URL must be set in environment variable")
        self.url_siru_endpoint = os.environ.get('SIRU_SERVICE_ENDPOINT')

    def run(self, params):
        # todo: on-demand run request
        pass

    def batch_run(self):
        """
        Evaluate the ICR SIRU model in batch mode for all targets and parameter combinations.
        :return:
        """
        df_params = self._get_model_params()
        proc_param_cols = ["MODEL_CONFIG_ID", "IFR_VAL", "INFECTIOUS (SYMPTOMATIC)", "INFECTIOUS (ASYMPTOMATIC)"]
        for level in self.TARGET_LEVELS:
            target_ids = self._get_targets(level)
            logger.info("Starting evaluation process of model " + str(self.get_model_id())
                        + " for all " + str(len(df_params.index)) + " parameter combinations on level " + str(
                level) + " ...")
            df_params[proc_param_cols] \
                .apply(lambda row: self.run_param_set(target_ids, level,
                                                      row[0],
                                                      row[1],
                                                      row[2],
                                                      row[3]), axis=1)
            logger.info("Done running model evaluation for all parameter combinations")

    def run_param_set(self, target_ids, target_level,
                      model_config_id, ifr, days_infectious_symptomatic, days_infectious_asymptomatic):
        logger.debug(" - Calling ICR service...")
        df_icr = self.call_icr_service(target_ids, target_level, ifr, self.DAYS_AGGR, self.GEO_AGGR, self.SMOOTHING)
        logger.debug(" - Writing ICR results to DB...")
        self.write_icr_to_db(df_icr, target_level, self.DAYS_AGGR, self.GEO_AGGR, ifr)
        logger.debug(" - Calling SIRU service...")
        df_siru = self.call_siru_service(target_ids, target_level, days_infectious_symptomatic,
                                         days_infectious_asymptomatic, ifr, self.DAYS_AGGR, self.GEO_AGGR)
        logger.debug(" - Received " + str(len(df_siru.index)) + " results from model")
        logger.debug(" - Writing SIRU results to DB...")
        self.write_siru_to_db(df_siru, int(model_config_id), target_level)
        logger.debug(" - Done processing this parameter combination")

    def call_icr_service(self, ids, level, ifr, days_aggr, geo_aggr, smoothing) -> DataFrame:
        body = {
            "IFR_BASE": ifr,
            "TARGET_ID": ids,
            "TARGET_LEVEL": level,
            "DAYS_AGGR": days_aggr,
            "GEO_AGGR": geo_aggr,
            "SMOOTHING": smoothing
        }
        response = api_calls.call_endpoint(url=self.url_icr_endpoint,
                                           body=body,  # json.dumps(body),
                                           mode='POST')
        # TODO: check if orient is "value" and not "table" (it is send by the service as orient="table")
        df_resp = read_json(response["body"], orient="table")
        df_resp.reset_index(level=0, inplace=True)
        exp_cols = ["ICR_CI_LOWER", "DATE", "ICR_EXP_VAL", "TARGET_ID", "ICR_CI_UPPER"]
        assert all(col in df_resp.columns for col in exp_cols), \
            "Missing columns in response: " + str([col for col in exp_cols if col not in df_resp.columns])
        # read_json parses date into a datetime...so convert it back to a date
        df_resp = df_resp.rename(columns={"DATE": "DATETIME"})
        df_resp["DATE"] = df_resp["DATETIME"].dt.date
        return df_resp[exp_cols]

    def call_siru_service(self, ids, level, days_infectious_symptomatic, days_infectious_asymptomatic, ifr, days_aggr,
                          geo_aggr) -> DataFrame:
        body = {
            "TARGET_ID": ids,
            "TARGET_LEVEL": level,
            "DAYS_INFECT_SYMP": int(days_infectious_symptomatic),
            "DAYS_INFECT_ASYMP": int(days_infectious_asymptomatic),
            "ICR_PARAMS": {
                "IFR_BASE": ifr,
                "DAYS_AGGR": days_aggr,
                "GEO_AGGR": geo_aggr
            }
        }
        response = api_calls.call_endpoint(url=self.url_siru_endpoint,
                                           body=body,  # json.dumps(body),
                                           mode='POST')
        # TODO: check if orient is "value" and not "table" (it is send by the service as orient="table")
        df_resp = read_json(response["body"], orient="table")
        df_resp.reset_index(level=0, inplace=True)
        exp_siru_result_fields = ["DATE", "TARGET_ID", "NEW_REPORTED", "ACTIVE_REPORTED", "SUM_REPORTED",
                                  "NEW_INFECTIONS", "ACTIVE_INFECTIONS", "SUM_INFECTIONS"]
        assert all(col in df_resp.columns for col in exp_siru_result_fields), \
            "Missing columns in response: " + str([col for col in exp_siru_result_fields if col not in df_resp.columns])
        # read_json parses date into a datetime...so convert it back to a date
        df_resp = df_resp.rename(columns={"DATE": "DATETIME"})
        df_resp["DATE"] = df_resp["DATETIME"].dt.date
        return df_resp[exp_siru_result_fields]

    @classmethod
    def get_model_id(cls):
        return 1  # "ICR_SIRU"

    def write_icr_to_db(self, df_icr, level, days_aggr, geo_aggr, ifr):
        exp_cols = {"TARGET_ID", "DATE", "ICR_EXP_VAL", "ICR_CI_LOWER", "ICR_CI_UPPER"}
        assert set(df_icr.columns) == exp_cols, "Invalid ICR dataframe for db writing. Expected columns " \
                                                + str(exp_cols) + ", but got " + str(df_icr.columns)
        if level == "COUNTY":
            tbl = ICR_RESULTS_TBL_COUNTY
        elif level == "STATE":
            tbl = ICR_RESULTS_TBL_STATE
        elif level == "COUNTRY":
            tbl = ICR_RESULTS_TBL_COUNTRY
        else:
            logger.error("Invalid level provided: " + str(level))
            raise ValueError("Invalid level: " + str(level))
        df_icr["DAYS_AGGR"] = days_aggr
        df_icr["GEO_AGGR"] = geo_aggr
        df_icr["IFR_BASE"] = ifr
        df_icr.rename(columns={
            "ICR_EXP_VAL": "EXP_VAL",
            "ICR_CI_LOWER": "CI_LOWER",
            "ICR_CI_UPPER": "CI_UPPER"
        }, inplace=True)
        df_icr = df_icr.astype({
            "TARGET_ID": int,
            "DAYS_AGGR": int,
        })
        try:
            logger.info("Deleting old ICR data...")
            self.db.delete_data(tbl, where=df_icr[ICR_RESULTS_TBL_KEYS], commit=False)
            logger.info("Writing new ICR data...")
            self.db.write_data(df_icr, tbl, commit=True)
            logger.info(str(len(df_icr.index)) + " ICR values have been inserted.")
        except Exception as e:
            logger.exception("Error was thrown - rolling back ICR result delete and write")
            self.db.rollback()
            raise DBQueryException() from e

    def write_siru_to_db(self, df_siru, model_config_id, level):
        exp_siru_result_fields = {"DATE", "TARGET_ID", "ACTIVE_REPORTED",
                                  "NEW_REPORTED", "SUM_REPORTED", "ACTIVE_INFECTIONS",
                                  "NEW_INFECTIONS", "SUM_INFECTIONS"}
        assert set(df_siru.columns) == exp_siru_result_fields, \
            "Invalid SIRU dataframe for db writing. Expected columns " + str(exp_siru_result_fields) + ", but got " \
            + str(df_siru.columns)
        df_siru["MODEL_CONFIG_ID"] = model_config_id
        df_siru["INSERT_DATE"] = today()
        # todo: change after db table change
        df_siru.rename(columns={
            "NEW_REPORTED": "CASES_NEW",
            "SUM_REPORTED": "CASES_SUM",
            "ACTIVE_REPORTED": "CASES_ACTIVE",
            "ACTIVE_INFECTIONS": "INFECTIONS_ACTIVE",
            "NEW_INFECTIONS": "INFECTIONS_NEW",
            "SUM_INFECTIONS": "INFECTIONS_SUM",
        }, inplace=True)
        df_siru = df_siru.astype({
            "TARGET_ID": int,
            "MODEL_CONFIG_ID": int,
            "CASES_NEW": int,
            "CASES_SUM": int,
            "CASES_ACTIVE": int,
            "INFECTIONS_ACTIVE": int,
            "INFECTIONS_NEW": int,
            "INFECTIONS_SUM": int
        })
        # determine correct result table to write into
        if level == "COUNTY":
            res_table = MODEL_RESULT_COUNTY_TBL
        elif level == "STATE":
            res_table = MODEL_RESULT_STATE_TBL
        elif level == "COUNTRY":
            res_table = MODEL_RESULT_COUNTRY_TBL
        else:
            raise ValueError("Level must be in {COUNTY, STATE, COUNTRY}")
        try:
            logger.debug("Archiving old model results...")
            self.db.call_procedure("ARCHIVE_MDL_RESULTS", [model_config_id, "'" + level + "'"], commit=False)
            logger.debug("Deleting old model results in results table...")
            self.db.delete_data(res_table, where="MODEL_CONFIG_ID = " + str(model_config_id), commit=False)
            logger.debug("Writing new model results...")
            self.db.write_data(df_siru, res_table, commit=True)
            logger.debug("Database interaction completed.")
        except Exception as e:
            logger.exception("Error was thrown - rolling back model result archival, delete and write")
            self.db.rollback()
            raise DBQueryException() from e
        finally:
            logger.info("SIRU DB write done")


if __name__ == '__main__':
    db = DBConnector()
    try:
        """r = ICR_SIRU_Runner(db)
        resp = r.call_icr_service([1001], "COUNTY", 0.00358, 7, True)
        r.write_icr_to_db(resp, "COUNTY", 7, True)"""
        df = DataFrame({
            "MODEL_ID": [1],
            "COUNTY_ID": [1001],
            "CASE_COUNT": [123],
            "DATE": [today()],
            "INSERT_DATE": [today()]
        })
        db.write_data(df, "SOR_MDL_RESULT")
    finally:
        db.close_connection()
