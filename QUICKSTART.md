# Quickstart
## Setup
### dotenv
First, create a .env file in the project folder which contains all required environment variables. 
The .env file is not part of the git repository so that passwords and stuff are not shared publicly. 

Currently, environment variables are primarily used for the database connection. You need to add entries such as:

`DB_USER=<e.g. postgres>
DB_PASSWORD=<...>
DB_HOST=<e.g. localhost>
DB_PORT=<e.g. 5432>
DB_NAME=<...>`

### app.py and app.yml
In order to change/add REST endpoints to your application, manipulate the `app.yml` file in the project directory.
To connect the defined endpoint with functionality, specify the function name in the app.yml endpoint definition:

`operationId: app.evaluate`

This requires a method called `evaluate` to exist in `app.py`. The easiest way to keep the file modular is to import
methods from other files from there, such as:

`from some_model.model import evaluate`

## Run
### With Docker

 * Install [docker](https://docs.docker.com/).
 * Install [docker-compose](https://docs.docker.com/compose/install/).
 * Run `docker-compose up`
 
### With Pyenv + Poetry
 * Install [pyenv](https://github.com/pyenv/pyenv) with [pyenv-installer](https://github.com/pyenv/pyenv-installer).
 * Run `pyenv install 3.8.2`
 * Install [poetry](https://python-poetry.org) by running `curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python`
 * Run `poetry install`
 * Run `poetry run python app.py`