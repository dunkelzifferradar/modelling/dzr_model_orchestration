from abc import ABC, abstractmethod

from dzr_shared_util.db.db_conn import DBConnector
from pandas import DataFrame

from consts import MODEL_PARAMS_VIEW, DIM_TBL_COUNTIES, DIM_TBL_STATES, DIM_TBL_COUNTRIES


class ModelRunner(ABC):
    TARGET_LEVELS = ["COUNTY", "STATE", "COUNTRY"]
    DIM_TABLES_PER_LEVEL = {
        "COUNTY": DIM_TBL_COUNTIES,
        "STATE": DIM_TBL_STATES,
        "COUNTRY": DIM_TBL_COUNTRIES
    }

    def __init__(self, db: DBConnector) -> None:
        self.db = db

    @abstractmethod
    def batch_run(self):
        pass

    def _get_model_params(self) -> DataFrame:
        """
        Get all sets of model params for this model.
        :return: DataFrame of param table without the model id
        """
        df_params = self.db.get_data(MODEL_PARAMS_VIEW, where="MODEL_ID = " + str(self.get_model_id()))
        df_params = df_params.drop(columns="MODEL_ID")
        raw_param_cols = ["MODEL_CONFIG_ID", "IFR_VAL", "PRM_TIME_VAL", "PRM_TIME_TYPE"]
        assert all(col in df_params.columns for col in raw_param_cols), "Columns are missing: " + str(
            [col for col in raw_param_cols if col not in df_params.columns])
        # pivot the key-value-esque params into proper columns
        unique_model_config_ids = df_params["MODEL_CONFIG_ID"].unique()
        df_pivot = DataFrame()
        for model_config_id in unique_model_config_ids:
            df_params_for_id = df_params[df_params["MODEL_CONFIG_ID"] == model_config_id]
            unique_ifrs = df_params_for_id["IFR_VAL"].unique()
            assert len(unique_ifrs) == 1, "Error: for one model config id, there cannot be different IFR values. " \
                                          "However, the following were found for id " + str(model_config_id) \
                                          + ": " + str(unique_ifrs)
            key_val_rows = {}

            def _register_key_val(row, key_val_rows):
                key_val_rows[row[1]] = row[0]
                return None

            df_params_for_id[["PRM_TIME_VAL", "PRM_TIME_TYPE"]].apply(lambda row: _register_key_val(row, key_val_rows),
                                                                      axis=1)
            dict_new_row = {"MODEL_CONFIG_ID": [model_config_id],
                            "IFR_VAL": [unique_ifrs[0]]
                            }
            for entry in key_val_rows.items():
                dict_new_row[entry[0]] = [entry[1]]
            df_new_row = DataFrame(dict_new_row)
            df_pivot = df_pivot.append(df_new_row)
        return df_pivot

    @abstractmethod
    def get_model_id(self):
        pass

    def _get_targets(self, level) -> list:
        assert isinstance(level, str), "Invalid level type: " + str(type(level))
        assert level in self.TARGET_LEVELS, "Invalid level: " + str(level)
        df_targets = self.db.get_data(self.DIM_TABLES_PER_LEVEL[level])
        return df_targets["ID"].to_list()
