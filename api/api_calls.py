import json
from json.decoder import JSONDecodeError
from typing import Union

import requests
from pandas import DataFrame


def call_endpoint(url: str, body: Union[DataFrame, dict] = None, query_params: dict = None, mode='GET'):
    assert mode in ["GET", "POST", "DELETE", "PATCH", "PUT"], "Unknown mode: " + str(mode)
    if mode == 'GET':
        assert body is None, "Body is not None - unimplemented in GET request. Please use POST or params."
        response = requests.get(url=url,
                                params=query_params)
    elif mode == 'POST':
        if body is not None:
            if isinstance(body, DataFrame):
                data = body.to_dict()
            elif isinstance(body, dict):
                data = body
            else:
                raise TypeError("Body must be of type Dataframe or dict")
        else:
            data = body
        response = requests.post(url=url,
                                 json=data,
                                 params=query_params)
    else:
        raise NotImplementedError("Mode " + mode + " not yet supported")
    return {
        "status_code": response.status_code,
        "text": response.text,
        "body": response.json()
    }
